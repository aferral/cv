docker build -t latex_docker .
docker_img=latex_docker
rm ./cv_es.pdf
rm ./cv_en.pdf
docker run --rm -v ${PWD}:/latex $docker_img /bin/sh -c -x "cd /latex && latexmk  -bibtex -xelatex cv_es.tex ; latexmk -c"
docker run --rm -v ${PWD}:/latex $docker_img /bin/sh -c -x "cd /latex && latexmk  -bibtex -xelatex cv_en.tex ; latexmk -c"
